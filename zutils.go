package zutils

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func Assert(b bool, err string) {
	if b == false {
		fmt.Println("Assertion failed ", err)
		os.Exit(10)
	}
}

func InStringsSlice(sub string, s []string) bool {
	//fmt.Println("ZZ", "SUB:",sub, "S:",s)
	for _, it := range s {
		if it == sub {
			return true
			//fmt.Println("ZZ in match", sub, s)
		}
	}
	return false
}

func GetFileName(name, suffix string) string {
	// TBD check if file exist - if yes, add running counter to name
	return name + suffix
}

func FileNameWithoutSuffix(fullName string) string {
	filename := filepath.Base(fullName)
	var extension = filepath.Ext(filename)
	return filename[0 : len(filename)-len(extension)]
}

func Assign_strings(field *[]string, value []string) {
	//fmt.Println("!!", strings.Join(value, ":"))
	concat := strings.Join(value, " ")
	concat = strings.Trim(concat, "{")
	concat = strings.Trim(concat, "}")
	lines := strings.Split(concat, ";")
	//	fmt.Println("!!", strings.Join(lines, ":"))
	for _, line := range lines {
		words := strings.Fields(line)
		if len(words) > 0 {
			//fmt.Println("??", len(words), ":", words, ":")
			*field = append(*field, line)
		}
	}
	//fmt.Println("!!", strings.Join(*field, ":"))
}
